## [Main Page](../README.md)
### Problem statement

Let the electric potential on $`(x=-1)`$ and $`(x=1)`$-planes be zero volt.

The volume between $`(x=-1)`$ and $`(x=1)`$-planes is filled with a volume charge 

having a density of $`\varepsilon \pi^2 \sin (\pi x)`$ $`\text {Coulomb/m}^3`$

- Show (in details) how to obtain an expression for Electric Potential in the area between $`(x=-1)`$ and $`(x=1)`$, $`V(x)`$ where $`-1<x<1`$.  
- Electric Field in the area between $`(x=-1)`$ and $`(x=1)`$, $`\bold E(x)`$ where $`-1<x<1`$.  
The answer must in the form of a Vector function of $`x`$.




